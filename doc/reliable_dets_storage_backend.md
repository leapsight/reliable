

# Module reliable_dets_storage_backend #
* [Function Index](#index)
* [Function Details](#functions)

<a name="index"></a>

## Function Index ##


<table width="100%" border="1" cellspacing="0" cellpadding="2" summary="function index"><tr><td valign="top"><a href="#delete_all-2">delete_all/2</a></td><td></td></tr><tr><td valign="top"><a href="#enqueue-2">enqueue/2</a></td><td></td></tr><tr><td valign="top"><a href="#fold-3">fold/3</a></td><td></td></tr><tr><td valign="top"><a href="#init-0">init/0</a></td><td></td></tr><tr><td valign="top"><a href="#update-3">update/3</a></td><td></td></tr></table>


<a name="functions"></a>

## Function Details ##

<a name="delete_all-2"></a>

### delete_all/2 ###

`delete_all(Reference, WorkIds) -> any()`

<a name="enqueue-2"></a>

### enqueue/2 ###

`enqueue(Reference, Work) -> any()`

<a name="fold-3"></a>

### fold/3 ###

`fold(Reference, Function, Acc) -> any()`

<a name="init-0"></a>

### init/0 ###

`init() -> any()`

<a name="update-3"></a>

### update/3 ###

`update(Reference, WorkId, WorkItems) -> any()`

