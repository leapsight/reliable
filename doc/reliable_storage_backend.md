

# Module reliable_storage_backend #

__This module defines the `reliable_storage_backend` behaviour.__<br /> Required callback functions: `init/0`, `enqueue/3`, `update/4`, `fold/4`, `delete_all/3`.

