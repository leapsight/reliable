

# Module reliable_riak_storage_backend #
* [Function Index](#index)
* [Function Details](#functions)

__Behaviours:__ [`reliable_storage_backend`](reliable_storage_backend.md).

<a name="index"></a>

## Function Index ##


<table width="100%" border="1" cellspacing="0" cellpadding="2" summary="function index"><tr><td valign="top"><a href="#delete_all-3">delete_all/3</a></td><td></td></tr><tr><td valign="top"><a href="#enqueue-3">enqueue/3</a></td><td></td></tr><tr><td valign="top"><a href="#fold-4">fold/4</a></td><td></td></tr><tr><td valign="top"><a href="#init-0">init/0</a></td><td></td></tr><tr><td valign="top"><a href="#update-4">update/4</a></td><td></td></tr></table>


<a name="functions"></a>

## Function Details ##

<a name="delete_all-3"></a>

### delete_all/3 ###

`delete_all(Reference, Bucket, WorkIds) -> any()`

<a name="enqueue-3"></a>

### enqueue/3 ###

`enqueue(Reference, Bucket, X3) -> any()`

<a name="fold-4"></a>

### fold/4 ###

`fold(Reference, Bucket, Function, Acc) -> any()`

<a name="init-0"></a>

### init/0 ###

`init() -> any()`

<a name="update-4"></a>

### update/4 ###

`update(Reference, Bucket, WorkId, WorkItems) -> any()`

